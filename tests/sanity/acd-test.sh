#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
#
# Copyright (C) 2015-2019 Jason A. Donenfeld <Jason@zx2c4.com>. All Rights Reserved.

# ┌─────────────────────┐    ┌─────────────────────┐
# │   $netns0           │    │   $netns1           │
# │                     │    │                     │
# │┌────────┐           │    │           ┌────────┐│
# ││  eth0  │───────────┼────┼───────────│  eth0  ││
# │├────────┴──────────┐│    │┌──────────┴────────┤│
# ││192.0.2.1/24       ││    ││192.0.2.1/24       ││
# ││ping               ││    ││ACD daemon         ││
# │└───────────────────┘│    │└───────────────────┘│
# └─────────────────────┘    └─────────────────────┘


set -e

exec 3>&1
export LANG=C
netns0="ell-test-$$-0"
netns1="ell-test-$$-1"
pretty() { echo -e "\x1b[32m\x1b[1m[+] ${1:+NS$1: }${2}\x1b[0m" >&3; }
pp() { pretty "" "$*"; "$@"; }
maybe_exec() { if [[ $BASHPID -eq $$ ]]; then "$@"; else exec "$@"; fi; }
n0() { pretty 0 "$*"; maybe_exec ip netns exec $netns0 "$@"; }
n1() { pretty 1 "$*"; maybe_exec ip netns exec $netns1 "$@"; }
ip0() { pretty 0 "ip $*"; ip -n $netns0 "$@"; }
ip1() { pretty 1 "ip $*"; ip -n $netns1 "$@"; }
slp() { read -t "$1" -N 1 || true; }

do_cleanup() {
	set +e
	exec 2>/dev/null
	pp pkill ell-acd-client
	pp ip netns del $netns1
	pp ip netns del $netns0
	pp rm /tmp/tmp/acd-$$.log
}

trap do_cleanup EXIT

pkill ell-acd-client || true
ip netns del $netns0 2>/dev/null || true
ip netns del $netns1 2>/dev/null || true

pp ip netns add $netns0
pp ip netns add $netns1

ip0 link set dev lo up
ip0 link add name eth0 type veth peer name eth0 netns $netns1
ip0 link set dev eth0 up
ip1 link set dev lo up
ip1 link set dev eth0 up
idx="$(ip1 -j link show dev eth0 | jq '.[0].ifindex')"
n1 ./ell-acd-client $idx 192.0.2.1 -d -D defend -n 1>/tmp/acd-$$.log 2>&1 &
slp 5
ip0 address add dev eth0 192.0.2.1/24
n0 ping 192.0.2.2 -I eth0 -c5 -w5 -q || true
slp 5
pp cat /tmp/acd-$$.log
pp grep 'IP 192.0.2.1 has been lost' /tmp/acd-$$.log

